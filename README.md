# Grupa 14 - genesis

#### Zadatak:
Kreirati više adresa te uz svaku adresu dokument i sliku. Kroz web sučelje omogućiti izmjenu slike i brisanje dokumenata te dodavanje novih pomoću RESTA. 

#### Članovi: 
Mate Stojić i Josip Pinjuh

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

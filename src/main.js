import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false;

// Bootstrap vue
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);

// Axios
import axios from 'axios';

const genesisApi = {
  install (Vue) {
    Vue.prototype.$api = axios.create({
      baseURL: 'http://212.39.115.5:8585/genesisrest.svc/v3.0/type/',
      auth: {
        username: 'student1',
        password: 'student1'
      }
    })
  }
};


Vue.use(genesisApi);

new Vue({
  router,
  render: h => h(App)
}).$mount('#app');
